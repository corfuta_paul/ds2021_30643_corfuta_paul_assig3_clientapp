package Client;

import MonitoredValueService.MonitoredValueService;
import MonitoredValueService.MonitoredValueException;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;
import org.jfree.chart.*;
import views.Login;

@Deprecated
@Configuration
public class HessianClient {

    @Bean
    public HessianProxyFactoryBean hessianInvoker() {
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
        invoker.setServiceUrl("https://demo-corfuta-paul.herokuapp.com/monitoring");
        invoker.setServiceInterface(MonitoredValueService.class);
        return invoker;
    }

    public static void main(String[] args) throws MonitoredValueException {
        System.setProperty("java.awt.headless", "false");
        MonitoredValueService service
                = SpringApplication.run(HessianClient.class, args)
                .getBean(MonitoredValueService.class);
        Login appFrame = new Login(service);
        appFrame.setVisible(true);
    }
}