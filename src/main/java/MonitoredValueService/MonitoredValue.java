package MonitoredValueService;

import java.io.Serializable;


public class MonitoredValue implements Serializable {
    private final String timeStamp;
    private final String energyConsumption;

    public MonitoredValue( String timeStamp, String energyConsumption) {
        this.timeStamp=timeStamp;
        this.energyConsumption=energyConsumption;
    }

    public String getEnergyConsumption() {
        return energyConsumption;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String toString(){
        String s="";
        s=s+timeStamp+" "+energyConsumption+"\n";
        return s;
    }

}
