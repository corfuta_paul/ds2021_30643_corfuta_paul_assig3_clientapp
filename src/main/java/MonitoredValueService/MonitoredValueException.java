package MonitoredValueService;

public class MonitoredValueException extends Exception {
    public MonitoredValueException(String message) {
        super(message);
    }
}