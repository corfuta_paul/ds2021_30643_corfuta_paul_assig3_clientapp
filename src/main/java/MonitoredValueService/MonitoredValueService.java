package MonitoredValueService;


import java.util.ArrayList;

public interface MonitoredValueService {
       ArrayList<MonitoredValue> monitorValue(String username, String password) throws MonitoredValueException;
}
