package views;

import MonitoredValueService.MonitoredValue;
import MonitoredValueService.MonitoredValueException;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AverageChart extends JFrame {

    ArrayList<MonitoredValue> monitoredValues;
    int days;
    private JButton back;
    ChartMenu chartMenu;

    public AverageChart(ArrayList<MonitoredValue> monitoredValues, int days,ChartMenu chartMenu) throws ParseException {
        this.monitoredValues=monitoredValues;
        this.days=days;
        this.chartMenu=chartMenu;
        initUI();
    }

    private void initUI() throws ParseException {

        setLayout(new BorderLayout());
        setContentPane(new JLabel(new ImageIcon("C:\\Users\\corfu\\Desktop\\Facultate anul IV\\Assignments_SD\\ds2021_30643_corfuta_paul_assig3_clientapp\\src\\main\\java\\Images\\chart1.jpg")));
        setLayout(new FlowLayout());

        XYDataset dataset = createDataset();
        JFreeChart chart = createChart(dataset);

        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
        chartPanel.setBackground(Color.gray);
        add(chartPanel);

        back = new JButton("Back");
        add(back);
        back.setSize(100, 40);
        back.setLocation(300, 450);

        back.addActionListener((ActionEvent event) -> {
            chartMenu.setVisible(true);
            this.setVisible(false);
        });

        pack();
        setTitle("Line chart average");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private XYDataset createDataset() throws ParseException {

        ArrayList<MonitoredValue> monitoredValuesList=new ArrayList<>();
        Date now = new Date();
        var serie= new XYSeries("Past week average consumption");

        String ceva = monitoredValues.toString();

        String[] arrOfStr = ceva.split("}, \\{|, |\\[\\{|=|}]");

        for(int i=0; i<arrOfStr.length-1;i=i+4){
            monitoredValuesList.add(new MonitoredValue(arrOfStr[i+2],arrOfStr[i+4]));
        }
        int[] freq=new int[24];
        for (MonitoredValue m:
                monitoredValuesList) {
            String timestamp = m.getTimeStamp().substring(0,19);
            SimpleDateFormat formatter6=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date6=formatter6.parse(timestamp);
            int hour = date6.getHours();

            freq[hour]=freq[hour]+Integer.parseInt(m.getEnergyConsumption());

        }
        for(int i=0;i<24;i++){
            freq[i]= freq[i] / 7;
            serie.add(i,freq[i]);
        }


        var dataset = new XYSeriesCollection();
        dataset.addSeries(serie);


        return dataset;
    }

    private JFreeChart createChart(XYDataset dataset) {

        JFreeChart chart = ChartFactory.createXYLineChart(
                "Past week average energy consumption",
                "Time",
                "Average Energy Consumption",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        XYPlot plot = chart.getXYPlot();

        var renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLACK);
        renderer.setSeriesStroke(0, new BasicStroke(2.0f));

        plot.setRenderer(renderer);
        plot.setBackgroundPaint(Color.white);

        plot.setRangeGridlinesVisible(true);
        plot.setRangeGridlinePaint(Color.BLACK);

        plot.setDomainGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.BLACK);

        chart.getLegend().setFrame(BlockBorder.NONE);

        chart.setTitle(new TextTitle("Past week average energy consumption",
                        new Font("Serif", java.awt.Font.BOLD, 18)
                )
        );

        return chart;
    }

}