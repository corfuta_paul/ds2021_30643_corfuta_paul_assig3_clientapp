package views;

import MonitoredValueService.MonitoredValueService;
import MonitoredValueService.MonitoredValue;
import MonitoredValueService.MonitoredValueException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.util.ArrayList;



public class ChartMenu extends JFrame {
    MonitoredValueService monitoredValueService;
    String username;
    ArrayList<MonitoredValue> monitoredValues ;


    private JLabel user;
    private JButton button1;
    private JButton button2;
    private JButton back;
    private JTextField daysField;
    private JLabel daysLabel;
    private JLabel errorL;

    public ChartMenu(MonitoredValueService monitoredValueService, String username, ArrayList<MonitoredValue> monitoredValues) {
        this.monitoredValueService = monitoredValueService;
        this.username=username;
        this.monitoredValues=monitoredValues;

        initUI();
    }
    private void initUI() {
        setLayout(new BorderLayout());
        setContentPane(new JLabel(new ImageIcon("C:\\Users\\corfu\\Desktop\\Facultate anul IV\\Assignments_SD\\ds2021_30643_corfuta_paul_assig3_clientapp\\src\\main\\java\\Images\\chart1.jpg")));
        setLayout(new FlowLayout());


        user = new JLabel(" ");
        user.setText(username);
        user.setForeground(Color.white);
        user.setOpaque(true);
        user.setBackground(Color.black);
        user.setFont(new Font(user.getFont().getName(),user.getFont().getStyle(),20));
        user.setHorizontalAlignment(JLabel.CENTER);
        add(user);
        user.setSize(250, 30);
        user.setLocation(220, 60);

        button1 = new JButton("Past days energy consumption");
        add(button1);
        button1.setSize(300, 40);
        button1.setLocation(150, 250);

        daysLabel = new JLabel("Days:");
        daysLabel.setFont(new Font("Arial", Font.BOLD, 14));
        daysLabel.setForeground(Color.WHITE);
        add(daysLabel);
        daysLabel.setSize(200, 20);
        daysLabel.setLocation(525, 220);

        daysField = new JTextField();
        add(daysField);
        daysField.setSize(100, 40);
        daysField.setLocation(500, 250);

        button2 = new JButton("Past week average energy consumption");
        add(button2);
        button2.setSize(300, 40);
        button2.setLocation(150, 350);

        back = new JButton("Back");
        add(back);
        back.setSize(100, 40);
        back.setLocation(300, 450);


        setTitle("Chart Menu");

        button1.addActionListener((ActionEvent event) -> {

            PastDaysChart lineChart = null;
            try {
                if(!daysField.getText().equals("")){
                    int d = Integer.parseInt(daysField.getText());
                    lineChart = new PastDaysChart(monitoredValues,d,this);
                    lineChart.setVisible(true);
                    this.setVisible(false);
                }
                else {
                    errorL.setText("You must complete the number of days.");
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

        });


        errorL = new JLabel();
        errorL.setForeground(Color.red);
        add(errorL);
        errorL.setSize(300, 20);
        errorL.setLocation(250, 300);

        button2.addActionListener((ActionEvent event) -> {

            try {
                AverageChart averageChart = new AverageChart(monitoredValues,7,this);
                averageChart.setVisible(true);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.setVisible(false);
        });

        back.addActionListener((ActionEvent event) -> {
           monitoredValues= new ArrayList<>();
            try {
                Login login = new Login(monitoredValueService);
                login.setVisible(true);
            } catch (MonitoredValueException e) {
                e.printStackTrace();
            }
            this.setVisible(false);

        });

        setLayout(null);
        setSize(700, 700);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

}
