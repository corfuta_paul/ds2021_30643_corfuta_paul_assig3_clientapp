package views;

import MonitoredValueService.MonitoredValueService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;

import MonitoredValueService.MonitoredValueException;
import MonitoredValueService.MonitoredValue;


public class Login extends JFrame{
    MonitoredValueService monitoredValueService;
    ArrayList<MonitoredValue> monitoredValues = new ArrayList<>();
    private JButton logB;
    private JLabel userL;
    private JLabel passL;
    private JTextField userT;
    private JTextField passT;
    private JLabel errorL;

    public Login(MonitoredValueService monitoredValueService) throws MonitoredValueException {
        this.monitoredValueService = monitoredValueService;
        initUI();
    }
    private void initUI() throws MonitoredValueException {
        setTitle("Login");
        setSize(700,700);

        setLayout(new BorderLayout());
        setContentPane(new JLabel(new ImageIcon("C:\\Users\\corfu\\Desktop\\Facultate anul IV\\Assignments_SD\\ds2021_30643_corfuta_paul_assig3_clientapp\\src\\main\\java\\Images\\chart1.jpg")));
        setLayout(new FlowLayout());

        setLayout(null);

        userL = new JLabel("Username: ");
        userL.setFont(new Font("Arial", Font.BOLD, 14));
        userL.setForeground(Color.WHITE);
        add(userL);
        userL.setSize(200, 20);
        userL.setLocation(150, 100);

        passL = new JLabel("Password: ");
        passL.setFont(new Font("Arial", Font.BOLD, 14));
        passL.setForeground(Color.WHITE);
        add(passL);
        passL.setSize(200, 20);
        passL.setLocation(150, 150);

        userT = new JTextField();
        add(userT);
        userT.setSize(200, 20);
        userT.setLocation(250, 100);

        passT = new JPasswordField();
        add(passT);
        passT.setSize(200, 20);
        passT.setLocation(250, 150);

        logB = new JButton("Login");
        add(logB);
        logB.setSize(100, 40);
        logB.setLocation(300, 250);

        logB.addActionListener((ActionEvent event) -> {
            try {

                monitoredValues = monitoredValueService.monitorValue(userT.getText(),passT.getText());
                if(monitoredValues!=null){
                    System.out.println(monitoredValues);
                    ChartMenu chartMenu = new ChartMenu(monitoredValueService,userT.getText(),monitoredValues);
                    this.setVisible(false);
                    chartMenu.setVisible(true);
                }
                else {
                    errorL.setText("The username or password is wrong");
                }
            } catch (MonitoredValueException e) {
                e.printStackTrace();
            }

        });

        errorL = new JLabel();
        errorL.setForeground(Color.red);
        add(errorL);
        errorL.setSize(300, 20);
        errorL.setLocation(250, 300);


        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }


}
